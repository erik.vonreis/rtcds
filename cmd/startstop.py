# start, stop and restart models
import click
from .util import list_host_models, check_model_in_host
from .log import log
import subprocess
import time
from .env import environment as env
from .kernel_module import is_module_loaded


def start_model(model: str):
    """
    Start a model
    :param model: the name of the model to start
    :return:
    """
    log(f"### starting {model}")
    if not check_model_in_host(model):
        log(f"*** error: {model} can't be run on this host")
    else:
        cmd = f"sudo systemctl start rts@{model}.target".split()
        result = subprocess.run(cmd)
        if result.returncode == 0:
            log(f"waiting for {model} to finish starting")
            time.sleep(env.START_DELAY)
            log(f"### {model} started")


def start_all_models():
    """
    Start all models meant to run on the current host
    :return:
    """
    models = list_host_models()
    for model in models:
        start_model(model)


def stop_model(model: str):
    """
    Stop a model
    :param model: the name of the model to stop
    :return:
    """
    log(f"### stopping {model}")
    cmd = f"sudo systemctl stop rts@{model}.target".split()
    result = subprocess.run(cmd)
    if result.returncode == 0:
        attempts = 0
        while is_module_loaded(model):
            time.sleep(1)
            attempts += 1
            if attempts >= 120:
                log(f"Timed out waiting for {model} to stop")
        log(f"### {model} stopped")


def stop_all_models():
    """
    Stop all models running on the system
    :return:
    """
    models = list_host_models()

    # reverse so that IOP is removed last
    models.reverse()
    for model in models:
        stop_model(model)


@click.command()
@click.argument("model", nargs=-1)
@click.option("--all", "all_", default=False, is_flag=True, help="start all models")
def start(model, all_):
    """
    start models

    MODEL: the name of the models to start
    """
    for m in model:
        if m.lower() != m:
            click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
        else:
            start_model(m)
    if all_:
        start_all_models()


@click.command()
@click.argument("model", nargs=-1)
@click.option("--all", "all_", default=False, is_flag=True, help="stop all models")
def stop(model, all_: bool):
    """
    stop models

    MODEL: the name of the models to stop
    """
    for m in model:
        if m.lower() != m:
            click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
        else:
            stop_model(m)
    if all_:
        stop_all_models()


@click.command()
@click.argument("model", nargs=-1)
@click.option("--all", "all_", default=False, is_flag=True, help="restart all models")
def restart(model, all_: bool):
    """
    restart models

    MODEL: the name of the models to restart
    """
    for m in model:
        if m.lower() != m:
            click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
        else:
            stop_model(m)
            start_model(m)
    if all_:
        stop_all_models()
        start_all_models()


def add_to_group(group: click.Group):
    """
    Add the start, stop and restart commands to the group
    :param group:
    :return:
    """
    group.add_command(start)
    group.add_command(stop)
    group.add_command(restart)