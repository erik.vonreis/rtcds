"""
build a model
"""
import os.path as path
from .log import log, error
import os
import subprocess
import click
from .env import environment as env


def create_build_directory():
    """
    Create the build directory if necessary

    :return: the build path
    """

    global env

    build_path = env.RCG_BUILDD
    if path.isdir(build_path):
        return build_path

    rcg_path = env.RCG_SRC

    log(f"creating build directory {build_path}")
    os.makedirs(build_path, mode=0o775)

    log(f"configuring build directory {build_path}")

    os.chdir(build_path)

    conf = path.join(rcg_path, 'configure')
    subprocess.run([conf])
    return build_path


# set some defaults from environ
kernel_mode_default = env.USE_KERNEL_MODELS
user_mode_default = not kernel_mode_default

class BuildArgs:
    """ Class to manage build arguments for rtcds"""
    def __init__(self, build_user_space, build_kernel_space, build_librts):
        self.build_user_space = build_user_space
        self.build_kernel_space = build_kernel_space
        self.build_librts = build_librts

    def get(self):
        """
        Return list of arguments to pass to make when building models
        :return:  a list of arguments for make <model>
        """
        args = []
        if self.build_user_space:
            args.append("RCG_BUILD_USP=YES")
        if self.build_librts:
            args.append("RCG_BUILD_LIBRTS=YES")
        if not self.build_kernel_space:
            args.append("RCG_BUILD_NO_KOBJ=YES")
        return args


def build_model(model: str, build_args: BuildArgs):
    """
    Build one model
    :param model: The name of the model
    :param user_space: when true, build user-mode model
    :param kernel_space: when true, build kernel-mode model
    :return:
    """
    global env
    build_path = env.RCG_BUILDD
    os.chdir(build_path)
    log(f"### building {model}")
    args = build_args.get()
    cmd = ['make', model] + args
    subprocess.run(cmd, env=env.get_environment())


def build_all(build_args: BuildArgs):
    """
    Build all models for the given host, or build all models in the whole IFO if run on a boot server or build server
    :param user_space: When true, build user space models
    :param kernel_space: When true, build kernel space models
    :return: None
    """
    global env
    os.chdir(env.RCG_BUILDD)
    log(f"### building all models")
    args = build_args.get()
    cmd = ['make', '-i', 'World'] + args
    subprocess.run(cmd, env=env.get_environment())


@click.command()
@click.argument("model", nargs=-1
                )
@click.option("-a", "--all", "all_", default=False, is_flag=True,
              help='build all models'
              )
@click.option("--userland/--no-userland", "--user-space/--no-user-space", "user_space",
              default=user_mode_default, help="do not build model in user space",
              )
@click.option("--librts", "build_librts", is_flag=True, show_default=True,
              default=False, help="Build the librts library and python bindings",
              )
@click.option("--kernel-space/--no-kernel-space", "kernel_space",
               help="do not build model in user space",
               default=kernel_mode_default)

def build(model, all_, user_space, kernel_space, build_librts):
    """
    Build one or more models

    MODEL: the name of the model to build
    """

    global env
    env.check_environment()
    if os.getuid() == 0:
        error("building as 'root' not allowed")

    build_path = create_build_directory()
    os.chdir(build_path)

    build_args = BuildArgs(build_user_space=user_space, build_kernel_space=kernel_space, build_librts=build_librts)

    for m in model:
        if m.lower() != m:
            click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
        else:
            build_model(m, build_args)
    if all_:
        build_all(build_args)


def add_to_group(group: click.Group):
    """
    Add the command as a subcommand to a click.Group
    :param group:
    :return: None
    """
    group.add_command(build)
