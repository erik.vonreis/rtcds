# install models for rtcds
import click
from .env import environment as env
from .log import log, error
import subprocess
import os


def install_model(model: str):
    """
    Install a model
    :param model: The name of the model
    :return: None
    """
    os.chdir(env.RCG_BUILDD)
    log(f"### installing {model}")
    cmd = ["make",f"install-{model}"]
    environ = env.get_environment()
    subprocess.run(cmd, env=environ)


def install_all():
    """
    Install every model for the host, or every model on the whole IFO if run on boot server or build server
    :return:
    """
    os.chdir(env.RCG_BUILDD)
    log(f"### installing all models")
    cmd = ["make",f"installWorld"]
    subprocess.run(cmd, env=env.get_environment())


@click.command()
@click.argument("model", nargs=-1
                )
@click.option("-a", "--all", "all_", default=False, is_flag=True,
              help='build all models'
              )
def install(model, all_):
    """
    install one or more models

    MODEL: the name of one or more models
    """
    env.check_environment()
    if os.getuid() == 0:
        error("installing as 'root' not allowed")
    for m in model:
        if m.lower() != m:
            click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
        else:
            install_model(m)
    if all_:
        install_all()


def add_to_group(group: click.Group):
    group.add_command(install)
