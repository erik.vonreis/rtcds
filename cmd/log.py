"""
Logging
"""
import click


def log(line):
    """
    Log a string as a line
    :param line:
    :return:
    """
    return click.echo(line)


def error(msg):
    """
    Print an error message and exit
    :param msg:
    :return:
    """
    raise click.ClickException(msg)
