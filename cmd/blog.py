"""blog (build log) command"""
import click
import subprocess
from .env import environment as env
import sys


@click.command()
@click.argument("model")
@click.option("-i","--info", is_flag=True,
              help="print paths to logs")
def blog(model, info):
    """
    show last build log for a model

    MODEL: the name of the model
    """
    log_file = f"{env.RCG_BUILDD}/{model}.log"
    err_file = f"{env.RCG_BUILDD}/{model}_error.log"
    if info:
        subprocess.run(f"ls -al {log_file} {err_file}", shell=True,
            stdout=sys.stdout, stderr=sys.stderr)
    else:
        try:
            with open(log_file, "rt") as f:
                sys.stdout.write(f.read())
            with open(err_file, "rt") as f:
                sys.stderr.write(f.read())
        except IOError as e:
            click.echo(str(e))


def add_to_group(group: click.Group):
    """
    Add the command as a subcommand to a click.Group
    :param group:
    :return: None
    """
    group.add_command(blog)