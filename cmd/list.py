import click
from .util import list_host_models


@click.command(name="list")
def list_():
    """list systems for host"""
    models = list_host_models()
    click.echo("\n".join(models))


def add_to_group(group: click.Group):
    """
    Add the command as a subcommand to a click.Group
    :param group:
    :return: None
    """
    group.add_command(list_)
