#!/usr/bin/python3

"""
Command line tool for manipulating LIGO CDS real-time system
"""

import click
from . import build
from . import env
from . import install
from . import startstop
from . import list as list_
from . import enable
from . import log_command
from . import blog
from . import lsmod
from . import status

@click.group()
@click.version_option(version="6.4.2")
def rtcds():
    pass


build.add_to_group(rtcds)
install.add_to_group(rtcds)
env.add_to_group(rtcds)
startstop.add_to_group(rtcds)
list_.add_to_group(rtcds)
enable.add_to_group(rtcds)
log_command.add_to_group(rtcds)
blog.add_to_group(rtcds)
lsmod.add_to_group(rtcds)
status.add_to_group(rtcds)

rtcds()
