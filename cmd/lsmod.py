"""lsmod command"""
import click
import subprocess
import sys
from .util import list_host_models, epics_only_model
from .env import environment as env


def list_modules():
    """send important modules to stdout

    return True if all expected modules were found.
    """

    kernel_module_names = "mbuf gpstime".split()

    kernel_version = subprocess.run(['uname', '-r'], capture_output=True, text=True)
    if kernel_version.returncode != 0:
        print("Warning - uname command returned with an error code, unable to retrieve kernel version.")
    else:
        try:
            major_version = int(kernel_version.stdout.split(".")[0].strip())
        except:
            print("Warning - Kernel version from uname command could not be parsed.")
            major_version = -1

        # Version 5+ is Debian 11, so we have additional kernel modules
        if major_version >= 5:
            kernel_module_names.append("rts_cpu_isolator")
            kernel_module_names.append("rts_logger")


    dolphin = f"dis_kosif dis_{env.DOLPHIN_GEN}_ntb dis_{env.DOLPHIN_GEN}_dma dis_irm dis_sisci dis_intel_dma".split()
    models = [m for m in list_host_models() if not epics_only_model(m)]

    not_loaded = "%-18s ***NOT LOADED**"
    allgood = True
    result = subprocess.run("lsmod", capture_output=True, text=True)
    if result.returncode == 0:
        lines = result.stdout.split("\n")
        # get dictionary keyed by module name
        # with value = to line in lsmod
        line_by_mod = {line.strip().split()[0]: line for line in lines
                       if len(line.strip()) > 0}
        for mod in kernel_module_names:
            if mod in line_by_mod:
                click.echo(line_by_mod[mod])
            else:
                allgood = False
                click.echo(not_loaded % mod)
        if env.IS_DOLPHIN_NODE:
            for mod in dolphin:
                if mod in line_by_mod:
                    click.echo(line_by_mod[mod])
                else:
                    allgood = False
                    click.echo(not_loaded % mod)
        if len(models) > 0:
            click.echo()
        for mod in models:
            if mod in line_by_mod:
                click.echo(line_by_mod[mod])
            else:
                allgood = False
                click.echo(not_loaded % mod)

    else:
        sys.stdout.write(result.stdout)
        sys.stderr.write(result.stderr)
    return allgood


@click.command()
def lsmod():
    """list of loaded rts kernel modules
    """
    list_modules()


def add_to_group(group: click.Group):
    """
    Add the command as a subcommand to a click.Group
    :param group:
    :return: None
    """
    group.add_command(lsmod)
