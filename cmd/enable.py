"""commands for enabling or disabling models in systemd"""
import click
import subprocess
import sys
from .env import environment as env

@click.command()
@click.argument("model", nargs=-1)
def enable(model: str):
    """Enable a model to start on boot

    MODEL: the model to enable.
    """
    for m in model:
        subprocess.run(f"sudo systemctl enable rts@{m}.target", shell=True,
                       stdout=sys.stdout, stderr=sys.stderr)


@click.command()
@click.argument("model", nargs=-1)
def disable(model: str):
    """Disable a model to start on boot

    MODEL: the model to enable.
    """
    for m in model:
        subprocess.run(f"sudo systemctl disable rts@{m}.target", shell=True,
                       stdout=sys.stdout)


def add_to_group(group: click.Group):
    """
    Add the command as a subcommand to a click.Group
    :param group:
    :return: None
    """
    if env.ALLOW_MODEL_ENABLE:
        group.add_command(enable)
    group.add_command(disable)